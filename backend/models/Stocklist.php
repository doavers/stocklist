<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sc_stocklist".
 *
 * @property integer $id
 * @property string $Name
 * @property integer $Quantity
 * @property string $Image
 * @property integer $marketable
 * @property integer $unittype_id
 * @property double $price
 * @property double $profit
 * @property integer $is_deleted
 * @property integer $kind_id
 *
 * @property ScIngredient[] $scIngredients
 * @property ScIngredient[] $scIngredients0
 * @property ScOrders[] $scOrders
 * @property ScKind $kind
 * @property ScUnittype $unittype
 */
class Stocklist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
      public $imageFile;
    public static function tableName()
    {
        return 'sc_stocklist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Quantity', 'Image','imageFile', 'marketable', 'unittype_id', 'price', 'is_deleted', 'kind_id'], 'required'],
            [['Quantity', 'marketable', 'unittype_id', 'is_deleted', 'kind_id'], 'integer'],
            [['price', 'profit'], 'double'],
            [['Name', 'Image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Name' => 'Name',
            'Quantity' => 'Quantity',
            'Image' => 'Image',
            'marketable' => 'Marketable',
            'unittype_id' => 'Unittype ID',
            'price' => 'Price',
            'profit' => 'Profit',
            'is_deleted' => 'Is Deleted',
            'kind_id' => 'Kind ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScIngredients()
    {
        return $this->hasMany(ScIngredient::className(), ['id_ingredient' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScIngredients0()
    {
        return $this->hasMany(ScIngredient::className(), ['id_product' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScOrders()
    {
        return $this->hasMany(ScOrders::className(), ['stocklist_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKind()
    {
        return $this->hasOne(ScKind::className(), ['id' => 'kind_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnittype()
    {
        return $this->hasOne(ScUnittype::className(), ['id' => 'unittype_id']);
    }
}
