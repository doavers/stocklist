<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Stocklist;

/**
 * StocklistSearch represents the model behind the search form about `backend\models\Stocklist`.
 */
class StocklistSearch extends Stocklist
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'marketable', 'unittype_id'], 'integer'],
            [['Name', 'Image','imageFile'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Stocklist::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'Quantity' => $this->Quantity,
            'marketable' => $this->marketable,
            'unittype_id' => $this->unittype_id,
            'price' => $this->price,
            'profit' => $this->profit,
        ]);

        $query->andFilterWhere(['like', 'Name', $this->Name])
            ->andFilterWhere(['like', 'Image', $this->Image]);

        return $dataProvider;
    }
}
