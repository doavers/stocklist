<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sc_logs".
 *
 * @property integer $id
 * @property integer $id_pesanan
 * @property integer $amount
 * @property string $date
 */
class Logs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sc_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pesanan', 'amount', 'date'], 'required'],
            [['id_pesanan', 'amount'], 'integer'],
            [['date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pesanan' => 'Id Pesanan',
            'amount' => 'Amount',
            'date' => 'Date',
        ];
    }
}
