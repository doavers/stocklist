<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sc_kind".
 *
 * @property integer $id
 * @property string $Name
 *
 * @property ScStocklist[] $scStocklists
 */
class Kind extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sc_kind';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name'], 'required'],
            [['Name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScStocklists()
    {
        return $this->hasMany(ScStocklist::className(), ['kind_id' => 'id']);
    }
}
