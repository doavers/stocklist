<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sc_table".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ScOrders[] $scOrders
 */
class Table extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $quality;
    public static function tableName()
    {
        return 'sc_table';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','quality'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Table Name',
            'quality'=>'Total Table'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScOrders()
    {
        return $this->hasMany(ScOrders::className(), ['table_id' => 'id']);
    }
}
