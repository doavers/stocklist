<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sc_ingredient".
 *
 * @property integer $id_product
 * @property integer $id_ingredient
 * @property integer $amount
 * @property integer $is_deleted
 *
 * @property ScStocklist $idIngredient
 * @property ScStocklist $idProduct
 */
class Ingredient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sc_ingredient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product', 'id_ingredient', 'amount', 'is_deleted'], 'required'],
            [['id_product', 'id_ingredient', 'amount', 'is_deleted'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_product' => 'Id Product',
            'id_ingredient' => 'Id Ingredient',
            'amount' => 'Amount',
            'is_deleted' => 'Is Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIngredient()
    {
        return $this->hasOne(ScStocklist::className(), ['id' => 'id_ingredient']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProduct()
    {
        return $this->hasOne(ScStocklist::className(), ['id' => 'id_product']);
    }
}
