<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sc_orders".
 *
 * @property integer $id
 * @property integer $table_id
 * @property integer $stocklist_id
 * @property integer $amount
 * @property integer $is_logged
 * @property ScStocklist $stocklist
 * @property ScTable $table
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sc_orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'table_id', 'stocklist_id', 'amount'], 'required'],
            [['id', 'table_id', 'stocklist_id', 'amount','is_logged'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table_id' => 'Table ID',
            'stocklist_id' => 'Stocklist ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStocklist()
    {
        return $this->hasOne(ScStocklist::className(), ['id' => 'stocklist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTable()
    {
        return $this->hasOne(ScTable::className(), ['id' => 'table_id']);
    }
}
