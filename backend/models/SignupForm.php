<?php
namespace backend\models;

use common\models\User;
use yii\base\Model;
use Yii;
use backend\models\AuthAssignment;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $auth;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['auth', 'required'],
            ['auth', 'string'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        $auths = '';
        if ($this->validate()) {
            $user = new User();
            $auths = $this->auth;
            $user->username = $this->username;
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save(false)) {
                $AuthAssignment = new AuthAssignment();
                $AuthAssignment->item_name = $auths;
                     $AuthAssignment->user_id=$user->id;
                     $AuthAssignment->save(false);
                return $user;
            }
        }

        return null;
    }
}
