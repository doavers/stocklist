<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Table */

$this->title = 'Table';
$this->params['breadcrumbs'][] = ['label' => 'Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="table-create">
	<div class="panel panel-default col-sm-3 col-md-offset-4">
	    <h1><?= Html::encode($this->title) ?></h1>

	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?> 

	   

	</div>
</div>
