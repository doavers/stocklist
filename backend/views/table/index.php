<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\widgets\DoGridView;
use common\widgets\DoPjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UnittypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Generate Tables';
$this->params['breadcrumbs'][] = $this->title;
$this->blocks['content-header'] = $this->title;
?>

<div class="unittype-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!-- <p>
        <?= Html::a('Add New', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

    <?php /*echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header'=>'No'],

           // 'id',
            'Name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);*/ ?>

    

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Actions</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body text-center">
            <div class="panel panel-default col-md-offset-2 col-sm-7 ">
                <h2>Add New Tables</h2>
                <p><?= Html::a('Add New', ['create'], ['class' => 'btn btn-success']) ?></p>
                <center><h3>============ OR ============</h3></center>
                <h2>Generate Tables</h2>
                <?= $this->render('generate', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>

    <?php DoPjax::begin(['id'=>'gridview-pjax', 'timeout' => false, 'enablePushState' => true]); ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List of <?= Html::encode($this->title) ?></h3>
                    <div class="box-tools">
                        <?= DoGridView::widget([
                            'id'            => 'gridview-summary',
                            'dataProvider'  => $dataProvider,
                            'filterModel'   => $searchModel,
                            'layout'        => "{summary}",
                        ]); ?>
                    </div>
                </div>

                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                
                <div class="box-body table-responsive no-padding">
                    <?= DoGridView::widget([
                        'id'            => 'gridview-items',
                        'dataProvider'  => $dataProvider,
                        'filterModel'   => $searchModel,
                        'layout'        => "{items}",
                        'columns'       => [
                            ['class' => 'yii\grid\SerialColumn'],
                            
                            'name',

                            [
                                'class'     => 'yii\grid\ActionColumn',
                                'header'    => '<center>Action</center>',
                                'template'  => '<center>{view} &nbsp; {update} &nbsp; {delete}</center>',
                                'contentOptions' => ['style'=>'min-width: 90px;'],
                            ],
                        ],
                    ]); ?>
                            
                </div>

                <div class="box-footer clearfix">
                    <?= DoGridView::widget([
                        'id'            => 'gridview-pager',
                        'dataProvider'  => $dataProvider,
                        'filterModel'   => $searchModel,
                        'layout'        => "{pager}",
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <?php DoPjax::end(); ?>
    
</div>