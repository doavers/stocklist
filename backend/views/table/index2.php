<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Table;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\TableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manage Table';
$this->params['breadcrumbs'][] = $this->title;
$this->blocks['content-header'] = $this->title;
?>
<div class="table-index">
<br>
<div class="body-content">

  <div style="overflow:auto;border-color:#000;background-color:#4b4b4b; height:445px; max-height:445px" class="panel row col-sm-10 col-md-offset-1">

        <div class="row" style="margin-left:6%; margin-top:4%">

        <div id="table" style="" >
            <?php
            $model = Table::find()->all();
            foreach ($model as $key) {
           
                echo '<a target="" class="showModalButton" style="float:left;width:120px;height:120px;" href="#"><span style="border-color:#a7f286;background-color:#a7f286;width:90px;height:90px" style="" class="btn btn-default"><div style="color:white;padding-top:20px;"><h5>'.$key->name.'<br/>available</h5></h5></div></span></a>';
           
            }
            ?>
        </div>


</div>

<script type="text/javascript">
        $(document).ready(function(){
          $(".row div").draggable({ 
    stack: "#set div",
      stop: function(event, ui) {
          var pos_x = ui.offset.left;
          var pos_y = ui.offset.top;
          var need = ui.helper.data("need");

          //Do the ajax call to the server
          $.ajax({
              type: "POST",
              url: "your_php_script.php",
              data: { x: pos_x, y: pos_y, need_id: need}
            }).done(function( msg ) {
              alert( "Data Saved: " + msg );
            }); 
      }
  }); });
</script>

