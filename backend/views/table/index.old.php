<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TableSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Table';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="table-index">
<div class="panel panel-default col-md-offset-2 col-sm-7 ">

<h4>Add New Tables</h4>
<p>
        <?= Html::a('Add New Tables', ['create'], ['class' => 'btn btn-success']) ?>
    </p><br/>   
    

    <center><h1>OR</h1></center><br/>
<h4>Generate Tables</h4>
    <?= $this->render('generate', [
            'model' => $model,
        ]) ?>
    </div>
   <!--  <div style="float:right" class="panel panel-default col-sm-3 ">
<h4>Add New Table</h4>

    <?= $this->render('add', [
            'model' => $model,
        ]) ?>
    </div> -->
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    
        

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //  'id',
            'name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?> 

</div>

