<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Table */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="table-form">
<br/>
    <?php $form = ActiveForm::begin(); ?>
 <?= $form->field($model, 'quality')->textInput([/*'onchange'=>'toggleSSN();'*/]) ?>

    <div class="form-group">
        <?= Html::submitButton('Generate', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
