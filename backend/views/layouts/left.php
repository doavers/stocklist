<?php

use yii\bootstrap\Nav;
use yii\widgets\Menu;
?>


<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <!-- <img src="<?= Yii::$app->homeUrl ?>/img/person.png" class="img-circle" alt="User Image"/> -->
            </div>
            <div class="pull-left info">
                <!-- <p>Side</p> -->

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>



        <?php
        echo Menu::widget([
            'options' => ['class' => 'sidebar-menu'],
            'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
            'activateParents' => true,
            'activateItems' => true,
            'encodeLabels' => false,
            'items' => [

                [
                    'label' => '<i class="glyphicon glyphicon-home"></i><span> Home</span> ',
                    'url' => ['site/index']
                ],
                [
                    'label' => '<i class="fa  fa-gears"></i><span> Manajemen </span><i class="fa fa-angle-left pull-right"></i>',
                    'url' => ['#'],
                    'items' => [['label' => 'Unit Type', 'url' => ['/unittype/index']],
                                    
                                     ['label' => 'Stock List', 'url' => ['/stocklist/index']],
                                     ['label' => 'Generate Table', 'url' => ['/table/generate ']],
                                      ['label' => 'Manage Table', 'url' => ['/table/index2']],
                                      
                                      ['label' => 'SignUp any User', 'url' => ['/site/signup']]
                    ]
                ],
                

//                [
//                    'label' => '<i class="glyphicon glyphicon-saved"></i><span> Gunakan Sensor </span>',
//                    'url' => ['sensorinuse/index'],
//                    'active' => Yii::$app->controller->id == "sensorinuse",
//                ],
//                [
//                    'label' => '<i class="fa   fa-bar-chart"></i><span> Data Hasil </span>',
//                    'url' => ['sensordata/index'],
//                    'active' => Yii::$app->controller->id == "sensordata",
//                ],
               

            ],
        ]);
        ?>




    </section>

</aside>
