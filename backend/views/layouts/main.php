<?php

use yii\helpers\Html;
include '../../vendor/dmstr/yii2-adminlte-asset/web/AdminLteAsset.php';
include '../../vendor/dmstr/yii2-adminlte-asset/widgets/Alert.php';
include '../../vendor/dmstr/yii2-adminlte-asset/helpers/AdminLteHelper.php';
include '../../vendor/rmrevin/yii2-fontawesome/AssetBundle.php';
include '../../vendor/kartik-v/yii2-money/MaskMoney.php';
include '../../vendor/kartik-v/yii2-money/MaskMoneyAsset.php';
/* @var $this \yii\web\View */
/* @var $content string */


if (Yii::$app->controller->action->id === 'login') {
    /**
     * Do not use this code in your template. Remove it. 
     * Instead, use the code  $this->layout = '//main-login'; in your controller.
     */
    echo $this->render(
            'main-login', ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>"/>
            <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
            <meta name="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
              <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
              <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
            <?= Html::csrfMetaTags() ?>
            <title><?= Html::encode($this->title) ?></title>
            <?php $this->head() ?>
        </head>
        <body class="hold-transition <?= \dmstr\helpers\AdminLteHelper::skinClass() ?> layout-boxed ">
            <!--<body class="hold-transition skin-blue  sidebar-mini">-->
            <?php $this->beginBody() ?>
            <div class="wrapper">

                <?=
                $this->render(
                        'header.php', ['directoryAsset' => $directoryAsset]
                )
                ?>

                <?=
                $this->render(
                        'left.php', ['directoryAsset' => $directoryAsset]
                )
                ?>

                <?=
                $this->render(
                        'content.php', ['content' => $content, 'directoryAsset' => $directoryAsset]
                )
                ?>

            </div>

            <?php $this->endBody() ?>
        </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
