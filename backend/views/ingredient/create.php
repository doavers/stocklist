<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use unclead\widgets\MultipleInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Ingredient */

$this->title = 'Create Ingredient';
$this->params['breadcrumbs'][] = ['label' => 'Ingredients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredient-create col-sm-5">

    <h1><?= Html::encode($this->title) ?></h1>

  <!--   <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
 -->

 <?php $form = ActiveForm::begin([
    'enableAjaxValidation'      => true,
    'enableClientValidation'    => false,
    'validateOnChange'          => false,
    'validateOnSubmit'          => true,
    'validateOnBlur'            => false,
]);?>

<?= $form->field($model, 'amount')->widget(MultipleInput::className(), [
    'limit' => 4,
    'columns' => [
        [
            'name'  => 'ingredient',
            //'type'  => 'dropDownList',
            'title' => 'User',
            // 'defaultValue' => 1,
            // 'items' => [
            //     1 => 'User 1',
            //     2 => 'User 2'
            // ]
        ],
        
        [
            'name'  => 'ingredient',
            'enableError' => true,
            'title' => 'Priority',
            'options' => [
                'class' => 'input-priority'
            ]
        ],
     
    ]
 ]);
?><?= Html::submitButton('Update', ['class' => 'btn btn-success']);?>
<?php ActiveForm::end();?>
</div>
