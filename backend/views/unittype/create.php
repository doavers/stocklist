<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Unittype */

$this->title = 'Add New';
$this->params['breadcrumbs'][] = ['label' => 'Unittypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class=" unittype-create">
	<div class="panel panel-default col-sm-3 col-md-offset-4">
	    <h1><?= Html::encode($this->title) ?></h1>

	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>

	</div>
</div>
