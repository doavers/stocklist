<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Unittype;
use kartik\widgets\SwitchInput;
use backend\models\Stocklist;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\models\Stocklist */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stocklist-form">
<div class=" col-sm-6">
    <?php $form = ActiveForm::begin(['id'=>'dynamic-form','options'=>['enctype'=>'multipart/form-data']]); ?>
    
    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>



   

    <div class="panel panel-success">
<div class="panel-body">
             <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 4, // the maximum times, an element can be cloned (default 999)
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $Ingredient[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'NIK',
                    'Nama',
                    'TanggalLahir',
                    'BulanLahir',
                    'TahunLahir',
                    'HubunganDenganKK',
                    'JenisKelamin',
                    'Agama',
                    'Pendidikan',
                    'Pekerjaan',
                    'StatusKawin',
                    'JKN',
                    'Mutasi'
                        
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer --><!-- widgetContainer -->
            <?php foreach ($Ingredient as $i => $Ingredients): ?>
                <div class="item panel panel-default"><!-- widgetBody -->
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Ingredients</h3>
                        <div class="pull-right">
                            <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                            <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                                        <div class="panel-body">
                        <?php
                            // necessary for update action.
                            if (!$Ingredients->isNewRecord) {
                                echo Html::activeHiddenInput($Ingredient, "[{$i}]id");
                            }
                        ?>
                        
                        <div class="row">
                            <div class="col-sm-6">
                           
                                
                                <?= $form->field($Ingredients, "[{$i}]id_ingredient")->dropDownList(ArrayHelper::map(Stocklist::find()->all(), 'id', 'Name') ) ?>
                                <?= $form->field($Ingredients, "[{$i}]amount")->textInput(['type'=>'number','maxlength' => true])->label('Qty') ?>

                            </div>
                           
                        </div><!-- .row -->
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
            
        
    </div><?php DynamicFormWidget::end(); ?>
    </div>


       
        
<br/>
    </div>    </div>
    <div class="  col-sm-6">
    

    <?= $form->field($model, 'profit')->textInput() ?>
    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <div class="form-group" style="float:right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>
    <br/>
    <br/>

    <?php ActiveForm::end(); 
            
    ?>

</div>
