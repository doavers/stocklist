  <?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use backend\models\Stocklist;
use backend\models\Unittype;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\StocklistSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stock list';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="stocklist-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Add Raw Product/ Instant Product', ['create'], ['class' => 'btn btn-success']) ?>
  Or
        <?= Html::a('Add Product with Ingredients', ['create2'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
      //  'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','header'=>'No'],

           // 'id',
            [ 
              //'class' => 'kartik\grid\ActionColumn',
              'header' => 'Image',
              'format' => 'html',
              'content' => function($model){
                $url = $model->Image;
                return Html::img('../'.$url,['width'=>'100','height'=>'100']);
              }

                    
              
          ],
         //   'Image',
            'Name',
            [
    'class' => 'kartik\grid\EditableColumn',
    'value' => function($model, $index, $column) {
        $service = Stocklist::find()->where(['id' => $model->id])->one()->marketable;
        if ($service == 1) {
            return 'marketable';
        } else if ($service == 0) {
            return 'not marketable';
        } },
    
    
        'attribute' => 'marketable',
        'pageSummary' => true,
            // 'format' => Editable::FORMAT_BUTTON,
//    'inputType' => Editable::INPUT_DROPDOWN_LIST,
     //'data'=>['Lunas'],
    'editableOptions' => ['inputType' => Editable::INPUT_DROPDOWN_LIST,'data'=>array('marketable'=>'marketable','not marketable'=>'not marketable')],
        //
        'readonly'=>function($model, $key, $index, $widget) {
            return ($model->marketable>1); // do not allow editing of inactive records
        },
      //  'editableValueOptions'=>['class'=>'text-danger']
        
  ],
          [
    'class' => 'kartik\grid\EditableColumn',
    'value' => function($model, $index, $column) {
        $service = Stocklist::find()->where(['id' => $model->id])->one()->Quantity;
        $unittype = Unittype::find()->where(['id'=>$model->unittype_id])->one()->Name;
          return $service.' '.$unittype;
       },
        'attribute' => 'Quantity',
        'pageSummary' => true,

            // 'format' => Editable::FORMAT_BUTTON,
//    'inputType' => Editable::INPUT_DROPDOWN_LIST,
     //'data'=>['Lunas'],
   // 'editableOptions' => ['inputType' => Editable::INPUT_DROPDOWN_LIST,'data'=>array('marketable'=>'marketable','not marketable'=>'not marketable')],
        //
        'readonly'=>function($model, $key, $index, $widget) {
           //  return ($model->marketable>1); // do not allow editing of inactive records
        },
      //  'editableValueOptions'=>['class'=>'text-danger']
        
  ],
            //'Quantity',
            //'Image',
            //'marketable',
            // 'unittype_id',
            // 'price',
            // 'profit',
        [

            'class' => 'kartik\grid\ActionColumn',
            'header' => 'Edit Ingredients',
            'template' => '{hh}{new_action2}',
            'buttons' => [
                'hh' => function ($url, $model) {

             
            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        'title' => Yii::t('app', 'Edit Ingredients'), 'class' => 'btn btn-success',
            ]);
            
        }
            ],
            'urlCreator' => function ($action, $model, $key, $index) {



        if ($action === 'hh') {

           $url = 'update?id='.$model->id;
            return $url;
        }
    }
        ],
          //  ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
