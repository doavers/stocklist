<?php

use common\widgets\DoGridView;
use common\widgets\DoPjax;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use backend\models\Stocklist;
use backend\models\Unittype;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\StocklistSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stock list';
$this->params['breadcrumbs'][] = $this->title;
$this->blocks['content-header'] = $this->title;
?>

<div class="stocklist-index">

    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Actions</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body text-right">
            <?= Html::a('<i class="fa fa-plus"></i> Add Raw Product/ Instant Product', ['create'], ['class' => 'btn btn-success']) ?>
            Or
            <?= Html::a('<i class="fa fa-plus"></i> Add Product with Ingredients', ['create2'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php DoPjax::begin(['id'=>'gridview-pjax', 'timeout' => false, 'enablePushState' => true]); ?>

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List of <?= Html::encode($this->title) ?></h3>
                    <div class="box-tools">
                        <?= DoGridView::widget([
                            'id'            => 'gridview-summary',
                            'dataProvider'  => $dataProvider,
                            'filterModel'   => $searchModel,
                            'layout'        => "{summary}",
                        ]); ?>
                    </div>
                </div>

                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                
                <div class="box-body table-responsive no-padding">
                    <?= GridView::widget([
                        'id'            => 'gridview-items',
                        'dataProvider'  => $dataProvider,
                        // 'filterModel'   => $searchModel,
                        'layout'        => "{items}",
                        'columns'       => [
                            ['class' => 'yii\grid\SerialColumn','header'=>'No'],
                            
                            // 'id',
                            [
                                //'class' => 'kartik\grid\ActionColumn',
                                'header' => 'Image',
                                'format' => 'html',
                                'content' => function($model) {
                                    $url = $model->Image;
                                    return Html::img('../'.$url,['width'=>'100','height'=>'100']);
                                }
                            ],
                            'Name',
                            [
                                'class' => 'kartik\grid\EditableColumn',
                                'value' => function($model, $index, $column) {
                                    $service = Stocklist::find()->where(['id' => $model->id])->one()->marketable;
                                    if ($service == 1) {
                                        return 'marketable';
                                    } else if ($service == 0) {
                                        return 'not marketable';
                                    } 
                                },
                                'attribute' => 'marketable',
                                'pageSummary' => true,
                                //'format' => Editable::FORMAT_BUTTON,
                                //'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                //'data'=> ['Lunas'],
                                'editableOptions' => ['inputType' => Editable::INPUT_DROPDOWN_LIST,'data'=>array('marketable'=>'marketable','not marketable'=>'not marketable')],
                                //
                                'readonly'=>function($model, $key, $index, $widget) {
                                    return ($model->marketable>1); // do not allow editing of inactive records
                                },
                                //  'editableValueOptions'=>['class'=>'text-danger']
                            ],
                            [
                                'class' => 'kartik\grid\EditableColumn',
                                'value' => function($model, $index, $column) {
                                    $service = Stocklist::find()->where(['id' => $model->id])->one()->Quantity;
                                    $unittype = Unittype::find()->where(['id'=>$model->unittype_id])->one()->Name;
                                    return $service.' '.$unittype;
                                },
                                'attribute' => 'Quantity',
                                'pageSummary' => true,

                                //'format' => Editable::FORMAT_BUTTON,
                                //'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                //'data'=>['Lunas'],
                                //'editableOptions' => ['inputType' => Editable::INPUT_DROPDOWN_LIST,'data'=>array('marketable'=>'marketable','not marketable'=>'not marketable')],
                                //
                                'readonly'=>function($model, $key, $index, $widget) {
                                    //return ($model->marketable>1); // do not allow editing of inactive records
                                },
                                //  'editableValueOptions'=>['class'=>'text-danger']
                            ],
                            //'Quantity',
                            //'Image',
                            //'marketable',
                            // 'unittype_id',
                            // 'price',
                            // 'profit',

                            [
                                'class' => 'kartik\grid\ActionColumn',
                                'header' => 'Edit Ingredients',
                                'template' => '{hh}{new_action2}',
                                'buttons' => [
                                    'hh' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                            'title' => Yii::t('app', 'Edit Ingredients'), 'class' => 'btn btn-success',
                                        ]);
                                    }
                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    if ($action === 'hh') {
                                        $url = 'update?id='.$model->id;
                                        return $url;
                                    }
                                }
                            ],
                        ],
                    ]); ?>
                            
                </div>

                <div class="box-footer clearfix">
                    <?= DoGridView::widget([
                        'id'            => 'gridview-pager',
                        'dataProvider'  => $dataProvider,
                        'filterModel'   => $searchModel,
                        'layout'        => "{pager}",
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

    <?php DoPjax::end(); ?>
    
</div>

