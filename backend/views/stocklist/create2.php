<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Stocklist */

$this->title = 'New Stock';
$this->params['breadcrumbs'][] = ['label' => 'Stocklists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stocklist-create">
	<div class="panel ">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form2', [
        'model' => $model,
        'Ingredient'=>$Ingredient,
    ]) ?>

</div></div>
