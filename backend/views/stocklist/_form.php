<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Unittype;
use backend\models\Kind;
use kartik\widgets\SwitchInput;
// include '../../vendor/kartik-v/yii2-money/MaskMoney.php';
use kartik\money\MaskMoney;
// include '../../vendor/kartik-v/yii2-money/MaskMoney.php';
// use kartik\widgets\MaskMoney;
//include '../../vendor/kartik-v/yii2-money/MaskMoney.php';
//include '../../vendor/kartik-v/yii2-money/MaskMoneyAsset.php';
/* @var $this yii\web\View */
/* @var $model backend\models\Stocklist */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="stocklist-form">
<div class=" col-sm-6">
    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
    
    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Quantity')->textInput() ?>

   

   

    <?= $form->field($model, 'unittype_id')->dropDownList(ArrayHelper::map(Unittype::find()->all(), 'id', 'Name') ) ?>
     <?= $form->field($model, 'marketable')->widget(SwitchInput::className(),[
    'name' => "marketable",'id'=>'markets',
    'pluginOptions' => [
        'size' => 'medium',
        'onColor' => 'success',
        'offColor' => 'danger',
        'onText' => 'yes',
        'onValue' => 1,
        'offText' => 'no',
        'offValue' => 0,
        ]
        ]) ?>
       
        
<br/>
    </div>
    <div class="  col-sm-6">
    <?= $form->field($model, 'price')->textInput();
 ?>

    <?= $form->field($model, 'profit')->textInput() ?>
    <?= $form->field($model, 'kind_id')->dropDownList(ArrayHelper::map(Kind::find()->all(), 'id', 'Name') ) ?>

    <?= $form->field($model, 'imageFile')->fileInput() ?>

    <div class="form-group" style="float:right">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>
    <br/>
    <br/>
    </div>
    <?php ActiveForm::end(); 
            
    ?>

</div>
