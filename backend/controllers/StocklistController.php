<?php

namespace backend\controllers;

use Yii;
use backend\models\Stocklist;
use backend\models\Ingredient;
use backend\models\StocklistSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use kartik\mpdf\Pdf;
use backend\models\Model;

/**
 * StocklistController implements the CRUD actions for Stocklist model.
 */
class StocklistController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Stocklist models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StocklistSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $model = new Stocklist();
        
                if(Yii::$app->request->post('hasEditable')){
           $Id = Yii::$app->request->post('editableKey');
     
           $model1 = $this->findModel($Id);
          
           $post = [];
           $posted = current($_POST['Stocklist']);
           $post['Stocklist'] = $posted;
          
          if (isset($posted['marketable'])) {
            if($posted['marketable']=='marketable'){
                    $model1->marketable = 1;

            }
            else if($posted['marketable']=='not marketable'){
                    $model1->marketable = 0;

            }
            
            //$output =  $model1->marketable;
          } 
          elseif (isset($posted['Quantity'])) {
            $model1->Quantity = $posted['Quantity'];
            //$output =  1;
          } 
          
          if($model1->save(false)){
            
          }
          // $out = Json::encode(['message'=>'sdf']);
         die($Id);
          echo $Id;
          return;
        }

      //  die($Id);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Stocklist model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Stocklist model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Stocklist();
        $modelIngredient = new Ingredient();

        if ($model->load(Yii::$app->request->post()) ) {
            $model->imageFile = UploadedFile::getInstance($model,'imageFile');
            $model->Image='\/images/'.$model->imageFile->name;
            
            $model->imageFile->saveAs("images/".$model->imageFile->name);
            $model->price = number_format($model->price, 2, '.', ',');
            $model->profit = number_format($model->profit, 2, '.', ',');
            if($model->save(false)){
            $modelIngredient->id_product = $model->id;
            $modelIngredient->id_ingredient = $model->id;
            $modelIngredient->amount = 1;

            $modelIngredient->save(false);
          }
            $searchModel = new StocklistSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->redirect(['index']);
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

        public function actionCreate2()
    {
      $qty =-1;
        $model = new Stocklist();
        $Ingredient = [new Ingredient];
        if ($model->load(Yii::$app->request->post())) {

          $model->marketable = 1;
          $model->unittype_id=1;
          $model->imageFile = UploadedFile::getInstance($model,'imageFile');
          $model->Image="\/images/".$model->imageFile->name;
          $model->price = number_format($model->price, 2, '.', ',');
            $model->profit = number_format($model->profit, 2, '.', ',');
          //$model->imageFrontend='/'."images/".$model->imageFile->name;
          $model->imageFile->saveAs("images/".$model->imageFile->name);
          $model->Quantity =1;
          $model->save(false);
          $Ingredient = Model::createMultiple(Ingredient::classname());
            Model::loadMultiple($Ingredient, Yii::$app->request->post());

            // // ajax validation
            // if (Yii::$app->request->isAjax) {
            //     Yii::$app->response->format = Response::FORMAT_JSON;
            //     return ArrayHelper::merge(
            //         ActiveForm::validateMultiple($Hubungandengankk),
            //         ActiveForm::validate($modelCustomer)
            //     );
            // }

            // validate all models
           // $valid = $model->validate();
            //$valid = Model::validateMultiple($Ingredient) && $valid;
//
           // if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($Ingredient as $Ingredients) {
                            $Ingredients->id_product = $model->id;
                            
                            $Ingredients->save();

                            //die(''.$Ingredients->amount.''.$Ingredients->id_ingredient);
                            // if (! ($flag = $Ingredients->save())) {
                            //     $transaction->rollBack();
                            //     break;
                            // }

//
                            //die(''.$Hubungandengankks->NomorKendali);
                        }
                        $countIngredint = Ingredient::find()->where(['id_product'=>$model->id])->all();
                        $counter = 0;
                        foreach ($countIngredint as $key) {
                          
                          $qtyTemp=0;
                          $product = Stocklist::find()->where(['id'=>$key->id_ingredient])->one()->Quantity;
                          if($key->amount< $product){
                            $qtyTemp=$product/$key->amount; 

                            if($qty==-1){
                              $qty = $qtyTemp;
                            }
                            elseif ($qty>$qtyTemp) {
                              $qty = $qtyTemp;
                            }
                          }
                        }
                    }
                    $model->Quantity = $qty;
                    $model->save(false);
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                  die(''.$Ingredients->amount.''.$Ingredients->id_ingredient);
                    //$transaction->rollBack();
                }
          //  }
             return $this->redirect(['/site/index']);
            
        } else {
            return $this->render('create2', [
                'model' => $model,
                'Ingredient' =>(empty($Ingredient))?[new Ingredient]:$Ingredient]);
           
        }
    }

    /**
     * Updates an existing Stocklist model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {
            $model->imageFile = UploadedFile::getInstance($model,'imageFile');
            $model->Image="\/images/".$model->imageFile->name;
            
            $model->imageFile->saveAs("images/".$model->imageFile->name);

            if($model->save(false)){
           
          }
            $searchModel = new StocklistSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'Ingredient' =>(empty($Ingredient))?[new Ingredient]:$Ingredient]);
            
        }
    }

    public function actionUpdate2($id)
    {
      $qty =-1;
        $model = $this->findModel($id);
        $Ingredient = [new Ingredient];
        if ($model->load(Yii::$app->request->post())) {
                      $model->marketable = 1;
          $model->unittype_id=1;
          $model->imageFile = UploadedFile::getInstance($model,'imageFile');
          $model->Image="\/images/".$model->imageFile->name;
          //$model->imageFrontend='/'."images/".$model->imageFile->name;
          $model->imageFile->saveAs("images/".$model->imageFile->name);
          $model->Quantity =1;
          $model->save(false);
          $Ingredient = Model::createMultiple(Ingredient::classname());
            Model::loadMultiple($Ingredient, Yii::$app->request->post());

            // // ajax validation
            // if (Yii::$app->request->isAjax) {
            //     Yii::$app->response->format = Response::FORMAT_JSON;
            //     return ArrayHelper::merge(
            //         ActiveForm::validateMultiple($Hubungandengankk),
            //         ActiveForm::validate($modelCustomer)
            //     );
            // }

            // validate all models
           // $valid = $model->validate();
            //$valid = Model::validateMultiple($Ingredient) && $valid;
//
           // if ($valid) {
                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    if ($flag = $model->save(false)) {
                        foreach ($Ingredient as $Ingredients) {
                            $Ingredients->id_product = $model->id;
                            
                            $Ingredients->save();

                            //die(''.$Ingredients->amount.''.$Ingredients->id_ingredient);
                            // if (! ($flag = $Ingredients->save())) {
                            //     $transaction->rollBack();
                            //     break;
                            // }

//
                            //die(''.$Hubungandengankks->NomorKendali);
                        }
                        $countIngredint = Ingredient::find()->where(['id_product'=>$model->id])->all();
                        $counter = 0;
                        foreach ($countIngredint as $key) {
                          
                          $qtyTemp=0;
                          $product = Stocklist::find()->where(['id'=>$key->id_ingredient])->one()->Quantity;
                          if($key->amount< $product){
                            $qtyTemp=$product/$key->amount; 

                            if($qty==-1){
                              $qty = $qtyTemp;
                            }
                            elseif ($qty>$qtyTemp) {
                              $qty = $qtyTemp;
                            }
                            else{
                              $qty=1;
                            }
                          }
                        }
                    }
                    $model->Quantity = $qty;
                    $model->save(false);
                    if ($flag) {
                        $transaction->commit();
                        return $this->redirect(['index']);
                    }
                } catch (Exception $e) {
                  die(''.$Ingredients->amount.''.$Ingredients->id_ingredient);
                    //$transaction->rollBack();
                }
          //  }
             return $this->redirect(['/site/index']);
        } else {
            return $this->render('update2', [
                'model' => $model,
                'Ingredient' =>(empty($Ingredient))?[new Ingredient]:$Ingredient,
            ]);
        }
    }

    /**
     * Deletes an existing Stocklist model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
      if($test =Ingredient::find()->where(['id_product'=>$id])->all()){

       // $test =Ingredient::find()->where(['id_product'=>$id,'id_ingredient'=>2])->one();
         foreach ($test as $key) {
            // $this->render(['/ingredient/delete'],['id_product'=>$key->id_product,'id_ingredient'=>$key->id_ingredient]);
          $testagain=new IngredientController($key->id_product,$key->id_ingredient);
          $testagain->actionDelete($key->id_product,$key->id_ingredient);
           //print_r($key->id_product);
           //die("".$key->id_ingredient);


         }
       }
        //print_r($test[0]->id_product);
        $this->findModel($id)->delete();


        return $this->redirect(['index']);
    }

    /**
     * Finds the Stocklist model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stocklist the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Stocklist::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
