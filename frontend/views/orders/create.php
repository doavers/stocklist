<?php

use yii\helpers\Html;
use frontend\models\Stocklist;
use frontend\models\Ingredient;
use frontend\models\Kind;
/* @var $this yii\web\View */
/* @var $model frontend\models\Orders */

$this->title = 'Create Orders';
// $this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div style="margin-left:-5%; " class="orders-create">
<?php echo $id ?>
<div style="" class="col-sm-4">
<div  class="  " style="padding-top:10px;padding-bottom:10px">
	
	<button style="color:#fff;background-color:#ebbf6d;width:30%"  class="btn " onclick="myFunctionCanceled()">Back</button>
	<button style="color:white;background-color:#35d374;float:right;width:30%"  class="btn " formtarget="_blank" onclick="myFunction()">Take Order</button>

</div>
<div style="background-color:#f0eeee" class="panel panel-default col-sm-12">
	
	<table id="tblSubTotal" width="100%">
		<tr>
			<td width="37%">
				<b>Item</b>
			</td>
			
			<td width="20%">
				<b>Qty</b>
			</td>
			
			<td width="37%">
				<b>SubTotal</b>
			</td>
		</tr>
		<?php 
		if(isset($orderId))
		{
			$totalOrder = 0;
			foreach ($orderList as $order) {
				echo '<tr  class="subtotaltr" id="subtotaltr-'.$order->id.'" >'.
					'<td style="overflow:hidden; max-width: 30px;" width="37%">'.$order->stocklist->Name.'</td>'.
					'<td id="subtotaltdqty-'.$order->id.'" width="20%">'.$order->amount.'</td>'.
					'<td id="subtotaltdprice-'.$order->id.'"  width="37%">'.number_format($order->stocklist->price,2,".",",").'</td></tr>';

				$totalOrder = $totalOrder + ((double)$order->amount*(double)$order->stocklist->price);
			}
		}
		?>
	</table>
	<!-- <table id="tblTotal" width="100%">
		<tr>
			<td width="63%">
				Total
			</td>
			<td width="37%">
				Total Value
			</td>
		</tr>
	</table> -->
	<hr style="border-color:#000;">	
	<div style="padding-bottom: 40px;">
		<div style="float:left;font-weight:bold;padding-right:20px;">Total &nbsp;&nbsp;&nbsp;&nbsp;USD</div><div id="TOTAL" style="float:right;font-weight:bold;padding-right:20px;"><?= number_format((isset($totalOrder) ? $totalOrder : "0"), 2,".",","); ?></div>
	</div>
</div>
<div style="background-color:#f0eeee" class="panel panel-default col-sm-12">
	<table  width="100%">
		<tr >
			<td width="15%">
				<!-- //<span class="glyphicon glyphicon-trash btn btn-danger"></span> -->
			</td>
			<td  width="37%">
				Item
			</td>
			<td width="5%">
				
			</td>
			<td width="5%">
				Qty
			</td>
			<td width="5%">
				
			</td>
			<td width="37%">
				Price
			</td>
		</tr>
	</table>
</div>

<div style="overflow:auto;height:270px;max-height:270px;margin-top:-4%;background-color:#f0eeee" class="panel panel-default col-sm-12">
	<table  id="testTable"  width="100%">
		<?php 
		if(isset($orderId))
		{
			foreach ($orderList as $order) {
				echo '<tr  class="testtr" id="tr-'.$order->id.'">
					<td width="15%" data-itemid="'.$order->id.'"><span id="'.$order->id.'" class="btn_remove glyphicon glyphicon-trash btn btn-danger"></span></td>
					<td style="overflow:hidden; max-width: 30px;" width="37%">'.$order->stocklist->Name.'</td><td width="5%"></td>
					<td id="tdqty-'.$order->id.'" width="5%">'.$order->amount.'</td>
					<td width="5%"></td><td id="tdprice-'.$order->id.'"  width="37%">'.number_format($order->stocklist->price,2,".",",").'</td></tr>';
			}
		}
		?>
	</table>
</div>

</div>
<br>
	<div style="background-color:#f0eeee;overflow:auto;margin-top:-1.5%;" class="panel panel-success col-sm-8" >
		<form style="margin:10px;">
			<label class="checkbox-inline" style="margin-right: 15px;">
				<input type="checkbox" id="SelectAll" name="Kind" value="Food" checked=""> Select All
			</label>
			<label class="checkbox-inline" style="margin-right: 15px;">
				<input type="checkbox" id="Food" name="Kind" value="Food" checked=""> Food
			</label>
			<label class="checkbox-inline" style="margin-right: 15px;">
				<input type="checkbox" id="Beverage" name="Kind" value="Beverage" checked > Beverage
			</label>
			<label class="checkbox-inline" style="margin-right: 15px;">
				<input type="checkbox" id="Toppings" name="Kind" value="Toppings" checked > Toppings
			</label>
			<label class="checkbox-inline" style="margin-right: 15px;">
				<input type="checkbox" id="Others" name="Kind" value="Others" checked > Others
			</label>
		</form>
	</div>

<div style="background-color:#4b4b4b;overflow:auto;max-height:460px;height:460px;margin-top:-1.5%;" class="panel panel-success col-sm-8" >
	<div style="margin-top:1%">
		<?php
            $models = Stocklist::find()->where(['marketable'=>1])->andWhere("Quantity>=1")->all();
            $arringred = [];
            $arramount = [];
            foreach ($models as $key) {
            	$ingred = Ingredient::find()->where(['id_product'=>$key->id])->all();
            	unset($arringred);
           		$arringred=[];
           		$arramount = [];
           		$arrquantity = [];
           		
            	foreach ($ingred as $keys) {            		
					array_push($arringred,$keys->id_ingredient);
					array_push($arramount,$keys->amount);
					foreach ($arringred as $idingredTemp) {
						array_push($arrquantity,Stocklist::find()->where(['id'=>$idingredTemp])->one()->Quantity);
					}
            	}

            	// print_r($arringred);
            	// echo '<div class="panel panels '.Kind::findOne(['id'=>$key->kind_id])->Name.'"  style="margin-left:1%;background-color:#f0eeee;float:left;width:150px;height:180px;"><a  class="btnAdd" data-price="'.$key->price.'" id="'.$key->id.'" data-name="'.$key->Name.'" style="float:left;width:150px;height:150px;" href=#><span style="margin-left:6%;width:130px;height:130px" class="col-md-offset-1 glyphicon  btn btn-default"><div style="overflow:hidden;"><img width="100%" height="117px" alt='.$key->Name.' src="'.Yii::$app->homeUrl.'/../../../backend/web/'.$key->Image.'"/><div style="overflow:hidden;"><span style="background-color:##fff;color:#2aa95d"  class="btn-small" style="vertical-align:middle;"><marquee><strong>&nbsp'.$key->Name.'&nbsp</strong></marquee></span></div></div></span></a><table ><tr><td></td></tr></table><div id="data-'.$key->id.'" data-idingredient='.implode(",",$arringred).' data-ingredient='.implode(",",$arramount).' data-amount='.implode(",",$arrquantity).'></div></div>';
            	echo '<div class="panel panels '.Kind::findOne(['id'=>$key->kind_id])->Name.'"  style="margin-left:1%;background-color:#f0eeee;float:left;width:150px;height:180px;"><a  class="btnAdd" data-price="'.$key->price.'" id="'.$key->id.'" data-name="'.$key->Name.'" style="float:left;width:150px;height:150px;" href=#><span style="margin-left:6%;width:130px;height:130px" class="col-md-offset-1 glyphicon  btn btn-default"><div style="overflow:hidden;"><img width="100%" height="117px" alt='.$key->Name.' src="'.Yii::getAlias('@backendurl').$key->Image.'"/><div style="overflow:hidden;"><span style="background-color:##fff;color:#2aa95d"  class="btn-small" style="vertical-align:middle;"><marquee><strong>&nbsp'.$key->Name.'&nbsp</strong></marquee></span></div></div></span></a><table ><tr><td></td></tr></table><div id="data-'.$key->id.'" data-idingredient="'.implode(",",$arringred).'" data-ingredient="'.implode(",",$arramount).'" data-amount="'.implode(",",$arrquantity).'"></div></div>';
            }

		?>
	</div>
</div>

<!-- 
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?> -->

</div>
<script type="text/javascript">
	$(document).ready(function() {
		$idifCanceled=["zero"];
		$qtyifCanceled=["zero"];
		uncheckAllCategory();
		setArrItem();

		$arrid = [];
		$arrqty =[];

		$('#SelectAll').click(function() {
			if(!$('#SelectAll').is(':checked')){
				console.log("uncheck all function");
				uncheckAllCategory();
			}
			if($('#SelectAll').is(':checked')){
				console.log("check all function");
				checkAllCategory();
			}
		});

		$('#Food').click(function() {
			if(!$('#Food').is(':checked')){
				//alert("unchecked");
				$('.Food').css( "display", "none" );
			}
			if($('#Food').is(':checked')){
				$('.Food').css( "display", "block" );
			}
		});

		$('#Beverage').click(function() {
			if(!$('#Beverage').is(':checked')){
				//alert("unchecked");
				$('.Beverage').css( "display", "none" );
			}
			if($('#Beverage').is(':checked')){
				$('.Beverage').css( "display", "block" );
			}
		});

		$('#Toppings').click(function() {
			if(!$('#Toppings').is(':checked')){
				//alert("unchecked");
				$('.Toppings').css( "display", "none" );
			}
			if($('#Toppings').is(':checked')){
				$('.Toppings').css( "display", "block" );
			}
		});

		$('#Others').click(function() {
			if(!$('#Others').is(':checked')) {
				//alert("unchecked");
				$('.Others').css( "display", "none" );
			}
			if($('#Others').is(':checked')) {
				$('.Others').css( "display", "block" );
			}
		});
			
		$('.btnAdd').click(function() {
			var qty = 0;
			var is_enought;
			$ingreData = [];
			$idingreData = [];
			$qtyData = [];
			var splits;
			var id = $(this).attr('id');
			var Name = $(this).data('name');
			var price =$(this).data('price');
			var ingre=""+($('#data-'+id).data('ingredient'));
			var idingre=""+($('#data-'+id).data('idingredient'));
			var qtytmp= ""+($('#data-'+id).data('amount'));
			//var qty=document.getElementById('qty['+id+']').value;
			var splits_idingre = idingre.split(',');
			var splits_ingre = ingre.split(',');
			var splits_qtytmp = qtytmp.split(',');
			$idingreData = $ingreData.concat(splits_idingre);
			$ingreData = $ingreData.concat(splits_ingre);
			$qtyData = $qtyData.concat(splits_qtytmp);
			
			for (var i=0; i<$ingreData.length; i++){
			 	if($.find("#testTable > tbody > tr.testtr")==0){
				 	if(parseFloat($ingreData[i])*1>$qtyData[i]){
				 		is_enought = 0;
				 		break;
				 	}
				 	else{
				 		is_enought=1;
				 	}
				}
				else{
					$('#testTable tr#tr-'+id.toString()).each(function() {
						var customertd = $(this).find('#tdqty-'+id).html();
					});
					if(parseFloat($ingreData[i])*parseFloat(customertd)>$qtyData[i]){
			 			is_enought = 0;
			 			break;
				 	}
				 	else{
				 		is_enought=1;
				 	}
				}
			}

			if(is_enought==0)
			{
				alert("Stock Not Enought");
			}
			else
			{
				console.log("is_enought != 0");
				//$arrqty = new Array();
				// or var arr = [];
				setArrItem();

				//	var qty = $('#qty['+Name+']').attr('value');
				if($.find("#testTable > tbody > tr#tr-"+id.toString())==false) {
					console.log("bb");
					$Total = parseFloat($('#TOTAL').html());
					console.log('total before: '+$Total);
					$idifCanceled=[];
					$qtyifCanceled=[];
					var newList ='<tr  class="testtr" id="tr-'+id.toString()+'"><td width="15%" data-itemid="'+id.toString()+'"><span id="'+id.toString()+'" class="btn_remove glyphicon glyphicon-trash btn btn-danger"></span></td><td style="overflow:hidden; max-width: 30px;" width="37%">'+Name+'</td><td width="5%"></td><td id="tdqty-'+id.toString()+'" width="5%">1</td><td width="5%"></td><td id="tdprice-'+id.toString()+'"  width="37%">'+parseFloat(price).toFixed(2)+'</td></tr>';
					$('#testTable').append(newList);

					var newSubTotal ='<tr  class="subtotaltr" id="subtotaltr-'+id.toString()+'" >'+
						'<td style="overflow:hidden; max-width: 30px;" width="37%">'+Name+'</td>'+
						'<td id="subtotaltdqty-'+id.toString()+'" width="20%">1</td>'+
						'<td id="subtotaltdprice-'+id.toString()+'"  width="37%">'+parseFloat(price).toFixed(2)+'</td></tr>';
					$('#tblSubTotal').append(newSubTotal);

					$arrid.push(''+id);
					console.log("$arrid:"+$arrid);

					$arrqty.push(parseFloat(1));
					console.log("$arrqty:"+$arrqty);

					$Total=$Total+(price*1);
					console.log('total after: '+$Total);

					$('#TOTAL').replaceWith($('<div id="TOTAL" style="float:right;font-weight:bold;padding-right:20px;">' + parseFloat($Total).toFixed(2) + '</div>'));
					var QtyFinal = [];
					var idTochange =[];
					for (var i =0;i< $ingreData.length; i++) {

			 			QtyFinal.push(($qtyData[i]-parseFloat($ingreData[i])*qty));
			 			idTochange.push($idingreData[i]);
					}
					myFunctionStock(QtyFinal,idTochange);
					$idifCanceled.push(id);
					$qtyifCanceled.push(qty);
				}
				else
				{
					console.log("cc");
					// $Total = $('#TOTAL').val();
					$Total = parseFloat($('#TOTAL').html());
					console.log('total before: '+$Total);
					console.log("$arrid:"+$arrid);
					console.log("$arrqty:"+$arrqty);
					var customertd=0;
					var pricetd;
					$('#testTable tr#tr-'+id.toString()).each(function() {
			    		customertd = $(this).find('#tdqty-'+id).html();
			    		var i=0;
						for(i=0;i<$arrid.length;i++){
							if(parseFloat($arrid[i])==id&&parseFloat($arrqty[i])==parseFloat(customertd)){
								//$arrid.splice(i,1);
								$arrqty[i]=parseFloat(customertd)+1;
								i=$arrid.length;
								i+=1;
							}
							else{
							//	alert($arrid[i]+"!="+id+" &&"+$arrqty[i]+"!="+customertd);
							}
						}
			    		customertd = parseFloat(customertd)+1;
			    		pricetd = $(this).find('#tdprice-'+id.toString()).html();    
			    		$Total = $Total +(1*parseFloat(pricetd));
			    		console.log('total after: '+$Total);
			 		});
					$('#tdqty-'+id.toString()).replaceWith($('<td id="tdqty-'+id.toString()+'" width="5%">'+(parseFloat(customertd))+'</td>'));
					$('#TOTAL').replaceWith($('<div id="TOTAL" style="float:right;font-weight:bold;padding-right:20px;">' + parseFloat($Total).toFixed(2) + '</div>'));
					$ingreData=[];
					//alert(id);

					var $SubTotal;
					var $stArrId = [];
					var $stArrQty = [];
					var qtyItem=0;
					// var priceItem;
					$('#tblSubTotal tr#subtotaltr-'+id.toString()).each(function() {
			    		qtyItem = $(this).find('#subtotaltdqty-'+id).html();
			    		console.log('qtyItem:'+qtyItem+', id:#subtotaltdqty-'+id);
			    		var i=0;
						for(i=0;i<$stArrId.length;i++){
							if(parseFloat($stArrId[i])==id&&parseFloat($stArrQty[i])==parseFloat(qtyItem)){
								//$stArrId.splice(i,1);
								$stArrQty[i]=parseFloat(qtyItem)+1;
								i=$stArrId.length;
								i+=1;
							}
							else{
							//	alert($stArrId[i]+"!="+id+" &&"+$stArrQty[i]+"!="+qtyItem);
							}
						}
			    		qtyItem = parseFloat(qtyItem)+1;
			    		// priceItem = $(this).find('#subtotaltdprice-'+id.toString()).html();
			    		$SubTotal = parseFloat(qtyItem)*parseFloat(pricetd);
			 		});
			 		$('#subtotaltdqty-'+id.toString()).replaceWith($('<td id="subtotaltdqty-'+id.toString()+'" width="20%">'+(parseFloat(qtyItem))+'</td>'));
			 		$('#subtotaltdprice-'+id.toString()).replaceWith($('<td id="subtotaltdprice-'+id.toString()+'" width="37%">'+(parseFloat($SubTotal).toFixed(2))+'</td>'));
				}
			}
		});

		$(document).on('click','.btn_remove',function() {
			var rmv=$(this).attr("id");
			var cell = "tdprice-"+rmv.toString();
			//var qty = "tdqty-"+rmv.toString();
			var qty=parseFloat($('#testTable #tdqty-'+rmv.toString()).html());
			var tds=parseFloat($('#testTable #'+cell).html());
			var totalval = parseFloat($('#TOTAL').html());
			var iteration = totalval-(tds*qty);
			var i=0;
			for(i=0;i<$arrid.length;i++){
				if(parseFloat($arrid[i])==parseFloat(rmv)&&parseFloat($arrqty[i])==parseFloat(qty)){
					$arrid.splice(i,1);
					$arrqty.splice(i,1);
					i=$arrid.length;
					i+=1;	
					
				}
				else{
					//alert($arrid[i]+"!="+rmv+" &&"+$arrqty[i]+"!="+qty);
				}
			}
			$('#TOTAL').replaceWith($('<div id="TOTAL" style="float:right;font-weight:bold;padding-right:20px;">' + parseFloat(iteration).toFixed(2) + '</div>'));
			$(this).closest('tr').remove();
			myFunctionStockRemove(rmv,parseFloat(qty));
			var indexid = jQuery.inArray(rmv, $idifCanceled);
			$idifCanceled.splice(indexid,1);
			$qtyifCanceled.splice(indexid,1);
			
			if($idifCanceled.length===0){
				$idifCanceled=["zero"];
				$qtyifCanceled=["zero"];
			}
			//$('#testTable #tr'+rmv).remove();

			// Delete row in sub total
			$('#subtotaltr-'+rmv.toString()).remove();
		});
	});
	
	function setArrItem()
	{
		if($.find("#testTable > tbody > tr.testtr")==0){
			console.log("aa1");
			$Total = 0;
			$arrid = [];
			$arrqty =[];
		}
		else {
			console.log("aa2");
			$arrid = [];
			$arrqty =[];
			var qtytd=0;
			var pricetd;
			$('#testTable > tbody > tr').each(function() {
				console.log("sequesnce +");
				// console.log($(this).html());
				idtd = $(this).find("td").data('itemid');
				qtytd = $(this).find('#tdqty-'+idtd).html();
				
				$arrid.push(''+idtd);
				$arrqty.push(''+qtytd);
				});
		}
		console.log("init $arrid:"+$arrid);
		console.log("init $arrqty:"+$arrqty);
	}

	function checkAllCategory()
	{
		$("input#SelectAll").prop("checked", true);
		$("input#Food").prop("checked", true);
		$('.Food').css( "display", "block" );
		$("input#Beverage").prop("checked", true);
		$('.Beverage').css( "display", "block" );
		$("input#Toppings").prop("checked", true);
		$('.Toppings').css( "display", "block" );
		$("input#Others").prop("checked", true);
		$('.Others').css( "display", "block" );
	}

	function uncheckAllCategory()
	{
		$("input#SelectAll").prop("checked", false);
		$("input#Food").prop("checked", false);
		$('.Food').css( "display", "none" );
		$("input#Beverage").prop("checked", false);
		$('.Beverage').css( "display", "none" );
		$("input#Toppings").prop("checked", false);
		$('.Toppings').css( "display", "none" );
		$("input#Others").prop("checked", false);
		$('.Others').css( "display", "none" );
	}

	function myFunction()
    {
    	setArrItem();
    	<?php 
    	if(isset($orderId))
		{
    	?>
	        $.ajax({
	            url: '<?= Yii::$app->request->baseUrl. '/orders/take-order-update?id='.$id.'' ?>',
	           	type: 'post',
	           	data: { arrid:$arrid,arrqty:$arrqty,_csrf : '<?=Yii::$app->request->getCsrfToken()?>'},
	           	success: function (data) {
					alert(data);
				}
	      	});
	    <?php
		}
		else
		{
		?>
			$.ajax({
	            url: '<?= Yii::$app->request->baseUrl. '/orders/take-order?id='.$id.'' ?>',
	           	type: 'post',
	           	data: { arrid:$arrid,arrqty:$arrqty,_csrf : '<?=Yii::$app->request->getCsrfToken()?>'},
	           	success: function (data) {
					alert(data);
				}
	      	});
		<?php
		}
	    ?>
    }

    function myFunctionCanceled()
    {
        $.ajax({
            url: '<?= Yii::$app->request->baseUrl. '/orders/canceled?id='.$id.'' ?>',
           type: 'post',
           data: {idcancel:$idifCanceled,qtycancel:$qtyifCanceled,_csrf : '<?=Yii::$app->request->getCsrfToken()?>'},
           success: function (data) {
 

           }

      });
    }

    function myFunctionStock(QtyFinal,idTochange)
    {
        $.ajax({
            url: '<?= Yii::$app->request->baseUrl. '/orders/test2?id='.$id.'' ?>',
           type: 'post',
           /*data: { Qty:QtyFinal,idtochange:idTochange,_csrf : '<?=Yii::$app->request->getCsrfToken()?>'},*/
           data: { Qty:1,idtochange:idTochange,_csrf : '<?=Yii::$app->request->getCsrfToken()?>'},
           success: function (data) {
             

           }
      	});
    }
	
	function myFunctionStockRemove(rmv,qty)
    {
        $.ajax({
            url: '<?= Yii::$app->request->baseUrl. '/orders/test3?id='.$id.'' ?>',
           	type: 'post',
           	data: {idToRemove:rmv,QtyToAdd:qty,_csrf : '<?=Yii::$app->request->getCsrfToken()?>'},
           	success: function (data) {
           }
      	});
    }
</script>
