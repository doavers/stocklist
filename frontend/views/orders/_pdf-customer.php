<?php

use yii\helpers\Html;
use yii\grid\GridView;
use frontend\models\Orders;
use frontend\models\Stocklist;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\GangguanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$Orders= Orders::find()->where(['id'=>$idorder])->all();

?>
<div style="height:500px; max-height:100%; width:500px; max-width:100%;font-size:1">
<h6>Table <?= $id?></h6>

<table style="width:300px;" class="col-sm-12" border="0">
<tr>
    <td>
        <strong>Orders</strong>
    </td>
    <td>
        <strong>Qty</strong>
    </td>
    <td>
        <strong>Price</strong>
    </td>
    <td>
        <strong>SubTotal</strong>
    </td>
</tr>
<?php
$total = 0;
foreach ($Orders as $key) {
    $price = Stocklist::find()->where(['id'=>$key->stocklist_id])->one()->price;
    $subTotal = $price*$key->amount;
    $total = $total+$subTotal;
    echo '<tr>
        <td>
        '.Stocklist::find()->where(['id'=>$key->stocklist_id])->one()->Name.'
        </td>
        <td>
        '.$key->amount.'
        </td>
        <td>
        '.number_format($price, 2, '.', ',').'
        </td>
        <td>
        '.number_format($subTotal, 2, '.', ',').'
        </td>
    </tr>
    ';
}

echo '<tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><b>Total : </b></td>
        <td><b>
        '.number_format($total, 2, '.', ',').'
        </b></td>
    </tr>';

?>
        
    </table>
    
 </div>
    <!-- <hr/> -->

