<?php

use yii\helpers\Html;
use frontend\models\Stocklist;
use frontend\models\Orders;
use frontend\models\Ingredient;

/* @var $this yii\web\View */
/* @var $model frontend\models\Orders */

$this->title = 'Order Report';
// $this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div style="" class="orders-create ">

<div style="border-color:#000;background-color:#4b4b4b" class="col-sm-8 col-md-offset-2">
	<div class="" style="margin-top:1%;padding-top:10px;margin-bottom:1%"><span style="width:100%" class="btn btn-danger"><strong>Take Order Report</strong></span></div>
<div style="" class="col-sm-7"><br/>
<!-- <button id='paymentbtn' class="btn btn-primary " onclick="myFunctionPay()">Save</button><br/> -->
	 

<div style="overflow:auto;margin-top:1%" class="col-sm-12 btn btn-default">
<span class=""><label>Transaction ID : <?= $id?></label></span>
</div>

<div class="panel panel-default col-sm-12" style="margin-top:1%;padding-top:10px;margin-bottom:1%">
<label>TOTAL (USD)</label>

	<h4 id="TOTAL" ><?php
		$sum =0;
		$ingredient = Orders::find()->where(['id'=>$id])->all();
		foreach ($ingredient as $key) {
			$sum+=((Stocklist::find()->where(['id'=>$key->stocklist_id])->one()->price)*($key->amount	));
		}

		echo number_format($sum, 2, '.', ',');
	?></h4>
</div>
<div class="panel panel-default col-sm-12" style="padding-top:5%;padding-bottom:5%">
	<?php //echo Html::a('Back', ['back', 'id' => $id,'idtable' => $idtable], ['class' => 'btn','style'=>'width:40%;background-color:#ebbf6d;color:#fff;']) ?>
	<?php //echo Html::a('Done', ['test1', 'id' => $id,'idtable' => $idtable], ['class' => 'btn btn-success','style'=>'width:40%;float:right']) 
	?>
	<?php echo Html::a('Back', ['update-order', 'id' => $idtable], ['class' => 'btn','style'=>'width:40%;background-color:#ebbf6d;color:#fff;']) ?>
	<?php echo Html::a('Print', 'javascript:void(0)', ['class' => 'btn btn-primary','style'=>'width:40%;float:right', 'onclick'=>"printFunction()"]) ?>
</div>
<!-- <div class="panel panel-danger col-sm-12" style="padding-top:10px;padding-bottom:0px"> -->
<!-- <form> -->
<!-- <label>Pay</label>
	<input id="inputPayment" type="text" name="payment" class="form-control">
</form>
<hr>
	<div id="totalPayment"></div>
</div>

 -->
	</div><div style="border-color:#000;background-color:#4b4b4b" class="col-sm-5"><br/>

<div style="margin-bottom:1%;margin-top:1%" class="panel panel-default col-sm-12">
	<table  width="100%">
		<tr >
			<td width="15%">
				<label>No</label><!-- //<span class="glyphicon glyphicon-trash btn btn-danger"></span> -->
			</td>
			<td  width="37%">
				<label>Item</label>
			</td>
			<td width="5%">
				
			</td>
			<td width="5%">
				<label>Qty</label>
			</td>
			<td width="5%">
				
			</td>
			<td width="37%">
				<label>Price</label>
			</td>
		</tr>
	</table>
</div>

<div style="overflow:auto;height:300px;" class="panel panel-default col-sm-12">
	<table  id="testTable"  width="100%">
		<?php
		$counter =1;
		
		foreach ($ingredient as $key) {
			echo '<tr class="testtr" id="tr-'.$key->id.'" ><td width="15%"><span id="'.$key->id.'" class=""><h4>'.$counter.'</h4></span></td><td style="overflow:hidden; max-width: 30px;" width="37%"><h4>'.Stocklist::find()->where(['id'=>$key->stocklist_id])->one()->Name.'</h4></td><td width="5%"></td><td id="tdqty-'.$key->id.'" width="5%"><h4>'.$key->amount.'</h4></td><td width="5%"></td><td id="tdprice-'.$key->id.'"  width="37%"><h4>'.number_format(Stocklist::find()->where(['id'=>$key->stocklist_id])->one()->price, 2, '.', ',').'</h4></td></tr>';
			$counter+=1;
			# code...
		}
		?>
	</table>
</div>


</div>
<br>



</div></div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#inputPayment').change(function(){
			$('#totalPayment').replaceWith($('<div id="totalPayment"><label>Residue</label><br/>' + (parseFloat($('#inputPayment').val())-parseFloat($('#TOTAL').text()))+ '<hr/></div>'));;
			//alert($('#TOTAL').text());
		});

		$('#paymentbtn').click(function(){
			if(parseFloat($('#totalPayment').text())>=0||$('#totalPayment').text()!=''){
    		//         $.ajax({
		    //         url: '<?= Yii::$app->request->baseUrl. '/orders/test1?id='.$id.'&idtable?='.$idtable.'' ?>',
		    //        type: 'post',
		    //        data: {test:'test'},
		    //        success: function (data) {
		    //           alert(data);

      //      }

      // });
    		alert('ready');
    	}
    	else{
			alert('Please Input Payment');
	    }	
		});
	});

	function myFunctionPay()
    {
    	  
    	
    	if((parseFloat($('#inputPayment').val())-parseFloat($('#TOTAL').text()))>=0||$('#totalPayment').text()!=''){
    		        $.ajax({
            url: '<?= Yii::$app->request->baseUrl. '/orders/test1?id='.$id.'&idtable?='.$idtable.'' ?>',
           type: 'post',
           data: {test:'test'},
           success: function (data) {
              alert(data);

           }

      });
    		
    	}
    	else{
			alert('Please Input Payment');
	    }	
    }

    function printFunction()
    {
    	// window.open("http://www.google.com");
    	// window.open ('<?= Yii::$app->request->baseUrl. '/orders/report?id='.$idtable.'&idorder='.$id.'' ?>', 'newwindow', config='height=400,width=400, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');
    	PopupCenter('<?= Yii::$app->request->baseUrl. '/orders/report?id='.$idtable.'&idorder='.$id.'' ?>','Print','900','500');  
    }

    function PopupCenter(url, title, w, h) {
	    // Fixes dual-screen position                         Most browsers      Firefox
	    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	    var top = ((height / 2) - (h / 2)) + dualScreenTop;
	    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

	    // Puts focus on the newWindow
	    if (window.focus) {
	        newWindow.focus();
	    }
	}
</script>
