<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
date_default_timezone_set('Asia/Bangkok');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

</style>
<SCRIPT type="text/javascript">    
             window.history.forward();
             function noBack() { 
                  window.history.forward(); 
             }
        </SCRIPT>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">
    <?php $this->beginBody() ?>
    <div class="wrap" style="">
         <?php
            NavBar::begin([
                'brandLabel' => 'Point of Sale',
                'brandUrl' => ['/site/index'],
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];}
            else{
                $menuItems[] = ['label' => ''.Date("Y-m-d H:i:s").'', 'url' => ['#'],'style'=>'color:#fff'];
                // $menuItems[] = ['label' => 'Sign Up', 'url' => ['/site/signup'],'style'=>'color:#fff'];
                $menuItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }   
           
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?> 

        <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        </div>
    </div>

   

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
