<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\StocklistSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Stocklists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stocklist-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Stocklist', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'Name',
            'Quantity',
            'Image',
            'marketable',
            // 'unittype_id',
            // 'price',
            // 'profit',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
