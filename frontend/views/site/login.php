<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login" >
<section id="title" class="emerald" >
        <div class="container">
            <div class="row">
                
                <!-- <div class="col-sm-6">
                    <ul class="breadcrumb pull-right">
                        <li><a href="index.html">Home</a></li>
                        <li class="active">Blog</li>
                    </ul>
                </div> -->
            </div><br/>
        </div>
    </section><!--/#title-->
 <div class="panel-body" >

    <div class="col-sm-5 col-md-offset-3" >

                    
                    <!-- <p>Pellentesque habitant morbi tristique senectus et netus et malesuada</p> -->
                <!-- </div> -->
    <div class="panel panel-default" style="padding-top:11%; padding-bottom:11%">
        <center><h1>Login</h1></center>
     <div class="panel-body" >
    <p>Please fill out the following fields to login:</p>

    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    <!--  // <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?> -->
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div></div></div></div></div>
