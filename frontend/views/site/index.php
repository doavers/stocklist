<?php
/* @var $this yii\web\View */

use frontend\models\Table;
$this->title = 'Point Of Sale';
?>
<div class="site-index">

   <!--  <div class="jumbotron">
        <h1>Congratulations!</h1>
    
        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div> -->

    <div class="body-content">
        <div class="row">
            <div style="overflow:auto;border-color:transparent;background-color:#ddd;height:445px;max-height:445px;padding-top: 15px;" class="panel col-sm-3">
                <div class="panel col-sm-12" style="border-color:transparent;height:245px;padding-left: 0px;padding-right: 0px;">
                    <div class="text-center" style="background-color:#5aba60;border-radius:4px;height:40px;padding-top:10px;">Description</div>
                    <div style="padding:15px;">Sample Description</div>
                </div>
                <div class="panel col-sm-12" style="border-color:transparent;height:135px;padding:0px;">
                    <div class="text-center" style="vertical-align:middle;">
                        <img src="<?= $this->theme->baseUrl ?>/images/logo.png" alt="Logo Doavers" class="logo-white" style="padding-top: 40px;">
                    </div>
                </div>
            </div>
            <div style="overflow:auto;border-color:transparent;background-color:#4b4b4b;padding-left:0;height:445px; max-height:445px; margin-left:20px;" class="panel col-sm-8 ">
                <div class="row" style="margin-left:0px; margin-top:0px;">
                    <div class="panel col-sm-12" style="border-color:#000;padding-left: 0px;padding-right: 0px;">
                        <div class="text-center" style="background-color:#b9595a;border-radius:4px;height:40px;padding-top:10px;color:#fff;">Tables</div>
                    </div>
                    <div class="col-sm-12" style="padding-left: 40px;padding-right: 0;" >
                        <?php
                        $model = Table::find()->all();
                        foreach ($model as $key) {
                            if($key->is_proccessed ==0){
                                echo '<a target="" class=".showModalButton" style="float:left;width:120px;height:120px;" href=../orders/create?id='.$key->id.'><span class="btn btn-default btn-table-avail"><div style="color:white;padding-top:20px;"><h5>'.$key->name.'<br/>available</h5></h5></div></span></a>';
                            }
                            elseif ($key->is_proccessed ==2) {
                                echo '<a target="" class=".showModalButton" style="float:left;width:120px;height:120px;" href=../orders/create2?id='.$key->is_used.'&idtable='.$key->id.'><span class="btn btn-default btn-table-done"><div style="color:white;padding-top:20px;"><h5>'.$key->name.'<br/>done</h5></h5></div></span></a>';
                            }
                            elseif($key->is_proccessed ==1){
                                // echo '<a target="" class=".showModalButton" style="float:left;width:120px;height:120px;" onclick="confimOnWaiting()" href=../orders/done?id='.$key->is_used.'&idtable='.$key->id.'><span class="btn btn-default btn-table-waiting"><div style="color:white;padding-top:20px;"><h5>'.$key->name.'<br/>waiting</h5></div></span></a>';
                                echo '<a target="" class="showmodal-waiting" style="float:left;width:120px;height:120px;" id="table-'.$key->id.'" data-tableid="'.$key->id.'" data-orderid="'.$key->is_used.'" href="javascript:void(0)"><span class="btn btn-default btn-table-waiting"><div style="color:white;padding-top:20px;"><h5>'.$key->name.'<br/>waiting</h5></div></span></a>';
                            }
                        }

                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="confimOnWaitingModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confim your action!</h4>
        </div>
        <div class="modal-body">
          <p>Do you want to <b>UPDATE ORDER</b> or <b>DONE ORDER</b>?</p>
        </div>
        <div class="modal-footer">
            <button id="modal-close" data-tableid="" type="button" class="btn btn-default" data-dismiss="modal">CLOSE</button>
            <button id="modal-update" data-tableid="" type="button" class="btn btn-default" >UPDATE</button>
            <!-- <button id="modal-print" data-tableid="" type="button" class="btn btn-default" >PRINT</button> -->
            <button id="modal-done" data-tableid="" type="button" class="btn btn-default" >DONE</button>
        </div>
      </div>
      
    </div>
</div>
<script type="text/javascript">
    $('.showmodal-waiting').click(function() {

        // onclick="confimOnWaiting()"
        var id = $(this).attr('id');
        var tableId = $(this).data('tableid');
        var orderId = $(this).data('orderid');
        console.log(id);
        console.log('tableId:'+tableId);
        console.log('orderId:'+orderId);
        $('#modal-update').replaceWith($('<button id="modal-update" data-tableid="'+tableId+'" data-orderid="'+orderId+'" type="button" class="btn btn-default" >UPDATE</button>'));
        $('#modal-print').replaceWith($('<button id="modal-print" data-tableid="'+tableId+'" data-orderid="'+orderId+'" type="button" class="btn btn-default" >PRINT</button>'));
        $('#modal-done').replaceWith($('<button id="modal-done" data-tableid="'+tableId+'" data-orderid="'+orderId+'" type="button" class="btn btn-default" >DONE</button>'));

        $('#confimOnWaitingModal').modal('show');
        modalConfirm(tableId, orderId);
        /*$.ajax({
            //orders/done?id='.$key->is_used.'&idtable='.$key->id.'
            url: '<?= Yii::$app->request->baseUrl. '/orders/done?id='.$key->is_used.'&idtable='.$key->id ?>',
            type: 'post',
            data: { arrid:$arrid,arrqty:$arrqty,_csrf : '<?=Yii::$app->request->getCsrfToken()?>'},
            success: function (data) {
                alert(data);
            }
        });*/
    // }
    });
    
    function modalConfirm(tableId, orderId) {
        $('#modal-update').click(function() {
            console.log('modal update clicked');
            // var tableId = $(this).data('tableid');
            $('#confimOnWaitingModal').modal('hide'); 
            var link = '<?= Yii::$app->request->baseUrl ?>'+'/orders/update-order?id='+tableId;
            console.log('link : '+link);
            window.location.href = link;
        });

        $('#modal-print').click(function() {
            console.log('modal print clicked');
            // var tableId = $(this).data('tableid');
            var link = '<?= Yii::$app->request->baseUrl ?>'+'/orders/report?id='+tableId+'&idorder='+orderId;
            console.log('link : '+link);
            PopupCenter(link,'Print','900','500');
        });

        $('#modal-done').click(function() {
            console.log('modal done clicked');
            // var tableId = $(this).data('tableid');
            $('#confimOnWaitingModal').modal('hide'); 
            var link = '<?= Yii::$app->request->baseUrl ?>'+'/orders/create2?id='+orderId+'&idtable='+tableId;
            console.log('link : '+link);
            window.location.href = link;
        });
    }

    function PopupCenter(url, title, w, h) {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    }

    // Auto refresh
    var time = new Date().getTime();
        $(document.body).bind("mousemove keypress", function(e) {
        time = new Date().getTime();
    });

    function refresh() {
        if(new Date().getTime() - time >= 60000) 
            window.location.reload(true);
        else 
            setTimeout(refresh, 10000);
    }

    setTimeout(refresh, 10000);
</script>