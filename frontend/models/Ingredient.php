<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sc_ingredient".
 *
 * @property integer $id_product
 * @property integer $id_ingredient
 * @property integer $amount
 *
 * @property ScUnittype $idIngredient
 * @property ScUnittype $idProduct
 */
class Ingredient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sc_ingredient';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product', 'id_ingredient', 'amount'], 'required'],
            [['id_product', 'id_ingredient', 'amount'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_product' => 'Id Product',
            'id_ingredient' => 'Id Ingredient',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIngredient()
    {
        return $this->hasOne(ScUnittype::className(), ['id' => 'id_ingredient']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProduct()
    {
        return $this->hasOne(ScUnittype::className(), ['id' => 'id_product']);
    }
}
