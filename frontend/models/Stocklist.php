<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sc_stocklist".
 *
 * @property integer $id
 * @property string $Name
 * @property float $Quantity
 * @property string $Image
 * @property integer $marketable
 * @property integer $unittype_id
 * @property double $price
 * @property double $profit
 *
 * @property ScOrders[] $scOrders
 * @property ScUnittype $unittype
 */
class Stocklist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sc_stocklist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name', 'Quantity', 'Image', 'marketable', 'unittype_id', 'price', 'profit'], 'required'],
            [['marketable', 'unittype_id'], 'integer'],
            [['Name', 'Image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Name' => 'Name',
            'Quantity' => 'Quantity',
            'Image' => 'Image',
            'marketable' => 'Marketable',
            'unittype_id' => 'Unittype ID',
            'price' => 'Price',
            'profit' => 'Profit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScOrders()
    {
        return $this->hasMany(ScOrders::className(), ['stocklist_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnittype()
    {
        return $this->hasOne(ScUnittype::className(), ['id' => 'unittype_id']);
    }
}
