<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sipp_r_pengguna".
 *
 * @property integer $pengguna_id
 * @property integer $id_user
 * @property string $username
 * @property string $password
 * @property string $lastlogin
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 * @property integer $deleted
 *
 * @property User $idUser
 * @property SippRRelasireviewuser[] $sippRRelasireviewusers
 * @property SippTReviewproposal[] $reviews
 * @property SippTPenelitian[] $sippTPenelitians
 * @property SippTPengumumanpenelitian[] $sippTPengumumanpenelitians
 * @property SippTProposalpenelitian[] $sippTProposalpenelitians
 */
class Pengguna extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sipp_r_pengguna';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pengguna_id'], 'required'],
            [['pengguna_id', 'id_user', 'deleted'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['username', 'password', 'lastlogin', 'created_by', 'updated_by'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pengguna_id' => 'Pengguna ID',
            'id_user' => 'Id User',
            'username' => 'Username',
            'password' => 'Password',
            'lastlogin' => 'Lastlogin',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSippRRelasireviewusers()
    {
        return $this->hasMany(SippRRelasireviewuser::className(), ['pengguna_id' => 'pengguna_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(SippTReviewproposal::className(), ['review_proposal_id' => 'review_id'])->viaTable('sipp_r_relasireviewuser', ['pengguna_id' => 'pengguna_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSippTPenelitians()
    {
        return $this->hasMany(SippTPenelitian::className(), ['id_pengguna' => 'pengguna_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSippTPengumumanpenelitians()
    {
        return $this->hasMany(SippTPengumumanpenelitian::className(), ['id_pengguna' => 'pengguna_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSippTProposalpenelitians()
    {
        return $this->hasMany(SippTProposalpenelitian::className(), ['id_pengguna' => 'pengguna_id']);
    }
}
