<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sc_unittype".
 *
 * @property integer $id
 * @property string $Name
 *
 * @property ScIngredient[] $scIngredients
 * @property ScIngredient[] $scIngredients0
 * @property ScStocklist[] $scStocklists
 */
class Unittype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sc_unittype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Name'], 'required'],
            [['Name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScIngredients()
    {
        return $this->hasMany(ScIngredient::className(), ['id_ingredient' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScIngredients0()
    {
        return $this->hasMany(ScIngredient::className(), ['id_product' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScStocklists()
    {
        return $this->hasMany(ScStocklist::className(), ['unittype_id' => 'id']);
    }
}
