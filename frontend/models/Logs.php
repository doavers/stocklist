<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sc_logs".
 *
 * @property integer $id
 * @property integer $id_pesanan
 * @property integer $amount
 * @property string $date
 * @property string $table_name
 */
class Logs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sc_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pesanan', 'amount', 'date', 'table_name'], 'required'],
            [['id_pesanan', 'amount'], 'integer'],
            [['date'], 'safe'],
            [['table_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_pesanan' => 'Id Pesanan',
            'amount' => 'Amount',
            'date' => 'Date',
            'table_name' => 'Table Name',
        ];
    }
}
