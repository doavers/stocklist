<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sc_orders".
 *
 * @property integer $id
 * @property integer $stocklist_id
 * @property integer $amount
 * @property integer $is_logged
 * @property integer $table_name
 * @property ScStocklist $stocklist
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sc_orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['stocklist_id', 'amount', 'table_name'], 'required'],
            //[['id', 'stocklist_id', 'amount', 'table_name'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stocklist_id' => 'Stocklist ID',
            'amount' => 'Amount',
            'table_name' => 'Table Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStocklist()
    {
        return $this->hasOne(Stocklist::className(), ['id' => 'stocklist_id']);
    }
}
