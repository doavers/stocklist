<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Orders;
use frontend\models\Ingredient;
use frontend\models\Stocklist;
use frontend\models\OrdersSearch;
use frontend\models\Table;
use frontend\models\TableSearch;
use frontend\models\Logs;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;


/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionTest2($id)
    {

        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        if(isset($_POST['Qty'])) {
            $counter = 0;
            foreach ($_POST['idtochange'] as $key) {
                $Model = Stocklist::find()->where(['id'=>$key])->one();
                return null;
                $Model->Quantity = $_POST['Qty'][$counter];
                $Model->save(false);
                $counter+=1;
            }
        }
        else{
            die("failed");
        }
        //return $this->redirect(['/site/index']);
        
    }
    
    public function actionPdf($id,$idorder)
    {
        
        // $searchModel = new GangguanSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('_pdf', ['id'=>$id,'idorder'=>$idorder
                   // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
            // 'bulan' => $bulan,
            // 'year'=>$year,
        ]);
       
    }

    public function actionPdfCustomer($id,$idorder)
    {
        
        // $searchModel = new GangguanSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('_pdf-customer', ['id'=>$id,'idorder'=>$idorder
                   // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
            // 'bulan' => $bulan,
            // 'year'=>$year,
        ]);
       
    }

    public function actionCanceled($id)
    {

        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        if(isset($_POST['idcancel'])){
           if(strcmp($_POST['idcancel'][0],"zero")==0){
                return $this->redirect(['/site/index']);
            }
            else
            {
                $counter =0;
                foreach ($_POST['idcancel'] as $keys) {
                    $Ingredient = Ingredient::find()->where(['id_product'=>$keys])->all();
                    foreach ($Ingredient as $key) {
                        $Model = Stocklist::find()->where(['id'=>$key->id_ingredient])->one();
                        $Model->Quantity = $Model->Quantity+($_POST['qtycancel'][$counter]*$key->amount);
                        $Model->save(false);
                    }
                    $counter+=1;
                }
                return $this->redirect(['/site/index']);
            }
            
        }
        else{
            die("failed");
        }
        //return $this->redirect(['/site/index']);
        
    }
    public function actionTest3($id)
    {

        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        if(isset($_POST['idToRemove'])){
            $modelIngredient = Ingredient::find()->where(['id_product'=>$_POST['idToRemove']])->all();

           foreach ($modelIngredient as $key) {
               $Model = Stocklist::find()->where(['id'=>$key->id_ingredient])->one();
               $Model->Quantity = $Model->Quantity+($_POST['QtyToAdd']*$key->amount);
               $Model->save(false);
           }     
                 
            }        
        else{
            die("failed");
        }
        //return $this->redirect(['/site/index']);
        
    }
    
    public function actionReportHtml($idorder,$idtable) {
        $model = new Orders();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'stocklist_id' => $model->stocklist_id]);
        } else {
            return $this->render('report-html', [
                'model' => $model,
                'id'=>$idorder,
                'idtable'=>$idtable,
            ]);
        }
    }

    public function actionReport($id,$idorder) {
     
        $content= $this->actionPdf($id,$idorder);
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
          // set to use core fonts only
          //'mode' => Pdf::MODE_CORE, 
           'mode' => Pdf::MODE_UTF8,
          // A4 paper format
           //'format' => Pdf::FORMAT_A4, 
           'format'=> [80,80],
          // portrait orientation
          'orientation' => Pdf::ORIENT_LANDSCAPE, 
          // stream to browser inline
          'destination' => Pdf::DEST_BROWSER, 
          // your html content input
          'content' => $content,  
          // format content from your own css file if needed or use the
          // enhanced bootstrap css built by Krajee for mPDF formatting 
          'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
          // any css to be embedded if required
          'cssInline' => '.kv-heading-1{font-size:18px}', 
           // set mPDF properties on the fly
          'options' => ['title' => 'Laporan'],
           // call mPDF methods on the fly
          'methods' => [ 
            'SetHeader'=>[''], 
            'SetFooter'=>['{PAGENO}'],
            ]
        ]);
        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }

    public function actionReportCustomer($id,$idorder) {
     
        $content= $this->actionPdfCustomer($id,$idorder);
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
          // set to use core fonts only
          //'mode' => Pdf::MODE_CORE, 
           'mode' => Pdf::MODE_UTF8,
          // A4 paper format
           //'format' => Pdf::FORMAT_A4, 
           'format'=> [80,80],
          // portrait orientation
          'orientation' => Pdf::ORIENT_LANDSCAPE, 
          // stream to browser inline
          'destination' => Pdf::DEST_BROWSER, 
          // your html content input
          'content' => $content,  
          // format content from your own css file if needed or use the
          // enhanced bootstrap css built by Krajee for mPDF formatting 
          'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
          // any css to be embedded if required
          'cssInline' => '.kv-heading-1{font-size:18px}', 
           // set mPDF properties on the fly
          'options' => ['title' => 'Laporan'],
           // call mPDF methods on the fly
          'methods' => [ 
            'SetHeader'=>[''], 
            'SetFooter'=>['{PAGENO}'],
            ]
        ]);
        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }

    public function actionTest1($id,$idtable)
    {

        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $Orders = Orders::find()->where(['id'=>$id])->all();

        foreach ($Orders as $key) {
            $model = $this->findModel($id, $key->stocklist_id);
            $ingredient = Ingredient::find()->where(['id_product'=>$key->stocklist_id])->all()  ;
            foreach ($ingredient as $ingredients) {
                //die("".$ingredients->id_ingredient);
                 $ingredient1 = Stocklist::find()->where(['id'=>$ingredients->id_ingredient])->one();
                 $ingredient1->Quantity = ($ingredient1->Quantity-$ingredients->amount);
                 $ingredient1->save(false);
            }

              $model->is_logged = 1;
              $model->save(false);
              
            //print_r($key->is_logged);
        }
        
        $table = Table::find()->where(['id'=>$idtable])->one();
        $table->is_used =0;
        $table->is_proccessed = 0;
        if($table->save(false)){
            $logs = new Logs();

            $logs->id_pesanan = $key->stocklist_id;
            $logs->amount = $key->amount;
            $logs->date = Date("Y-m-d H:i:s");
            $logs->table_name = $idtable;
            $logs->save(false);

            return $this->redirect(['/site/index']);
        }
        
    }
    public function actionDone($id,$idtable)
    {
        $table = Table::find()->where(['id'=>$idtable])->one();
        $table->is_proccessed = 2;
        $table->save(false);
              
            //print_r($key->is_logged);
        
        return $this->redirect(['/site/index']);
        
    }

    public function actionTakeOrder($id)
    {
        $rand = rand (1 , 999999999 );
        // if($this->findModel($rand)){
        //     do{
        //         $rand = rand (1 , 999999999 );
        //     }while($this->findModel($rand));
        // }
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // printr($_POST['arrqty']);
        if(isset($_POST['arrqty']) && isset($_POST['arrid'])){    
             $length = count($_POST['arrqty']);
           // die("success");
            for ($i=0; $i <$length ; $i++) { 
                $models = new Orders();
                $models->id = $rand;
                $models->stocklist_id = $_POST['arrid'][$i];
                $models->amount = $_POST['arrqty'][$i];
                $models->table_name= $id;
                $models->save(false);
            }
            
             $table=Table::find()->where(['id'=>$id])->one();
           
             $table->is_used =$models->id;
             $table->is_proccessed = 1;
             $table->save(false);
           

        // return $this->redirect(['report?id='.$id.'&idorder='.$models->id]);
        return $this->redirect(['report-html?idorder='.$models->id.'&idtable='.$models->table_name]);
          
        }
        else
            die("Something Wrong");
    }

    public function actionTakeOrderUpdate($id)
    {
        if(isset($_POST['arrqty']) && isset($_POST['arrid'])){    
            $length = count($_POST['arrqty']);
            
            // Get current order id 
            $orderId = Table::findOne($id)->is_used;
            // Delete all order with last generated id
            Orders::deleteAll(['id'=>$orderId]);

            for ($i=0; $i <$length ; $i++) 
            {
                // insert data
                $newItem = new Orders();
                $newItem->id = $orderId;
                $newItem->stocklist_id = $_POST['arrid'][$i];
                $newItem->amount = $_POST['arrqty'][$i];
                $newItem->table_name= $id;
                $newItem->save(false);
                
                /*$model = Orders::find()->where([
                    'id'=>$orderId, 
                    'stocklist_id'=>$_POST['arrid'][$i]
                ]);

                if($model->count() == 0)
                {
                    // insert data
                    $newItem = new Orders();
                    $newItem->id = $orderId;
                    $newItem->stocklist_id = $_POST['arrid'][$i];
                    $newItem->amount = $_POST['arrqty'][$i];
                    $newItem->table_name= $id;
                    $newItem->save(false);
                }
                else
                {
                    // update data
                    $model->amount = $_POST['arrqty'][$i];
                    $model->save(false);
                }*/
            }

            return $this->redirect(['report-html?idorder='.$orderId.'&idtable='.$id]);
        }
        else
            die("Something Wrong");
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @param integer $stocklist_id
     * @return mixed
     */
    public function actionView($id, $stocklist_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, $stocklist_id),
        ]);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Orders();

        if (Table::findOne($id)->is_used==0) {
           
            return $this->render('create', [
                'model' => $model,
                'id'=>$id,
            ]);
        }
        else{
            return $this->redirect(['/site/index']);
        }
        
    }

    public function actionCreate2($id,$idtable)
    {
        $model = new Orders();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'stocklist_id' => $model->stocklist_id]);
        } else {
            return $this->render('create2', [
                'model' => $model,
                'id'=>$id,
                'idtable'=>$idtable,
            ]);
        }
    }

    public function actionUpdateOrder($id)
    {
        $model = new Orders();
        $orderId = Table::findOne($id)->is_used;
        if ($orderId != 0) {
            $orderList = Orders::find()
                ->select('o.*, s.*')
                ->from(['o' => Orders::tableName()])
                ->joinWith([
                    'stocklist' => function($q){
                        $q->from(['s' => Stocklist::tableName()]);
                    }
                ])
                ->where(['o.id'=>$orderId])
                ->all();

            // var_dump($orderList); die();
            return $this->render('create', [
                'model' => $model,
                'id'=>$id,
                'orderId' => $orderId,
                'orderList' => $orderList,
            ]);
        }
        else{
            return $this->redirect(['/site/index']);
        }
        
    }

    public function actionBack($id,$idtable)
    {
        

        return $this->redirect(['/site/index']);
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param integer $stocklist_id
     * @return mixed
     */
    public function actionUpdate($id, $stocklist_id)
    {
        $model = $this->findModel($id, $stocklist_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'stocklist_id' => $model->stocklist_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param integer $stocklist_id
     * @return mixed
     */
    public function actionDelete($id, $stocklist_id)
    {
        $this->findModel($id, $stocklist_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param integer $stocklist_id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $stocklist_id)
    {
        if (($model = Orders::findOne(['id' => $id, 'stocklist_id' => $stocklist_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
