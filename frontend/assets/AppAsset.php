<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
        'js/ajax-modal-popup.js',
        'js/bootstrap.min.js',
        'js/html5shiv.js',
        'js/jquery.isotope.min.js',
        'static/jquery.js',
        'js/jquery.prettyPhoto.js',
        'js/main.js',
        'js/respond.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions =[
    'position' => \yii\web\View::POS_HEAD
    ];
}
