<?php
/**
 * @link https://doavers.com/
 * @copyright Copyright (c) 2015 Doavers Light
 * @license http://opensource.org/licenses/BSD-3-Clause
 */
namespace common\widgets;

class DoDataColumn extends \yii\grid\DataColumn
{
    // customize by changing your styles
    public $filterInputOptions = ['class' => 'form-control', 'id' => null];
    // customize by changing your styles
    public $sortLinkOptions = [];
}