<?php

namespace common\widgets;

class DoGridView extends \yii\grid\GridView 
{
    // change the following line
    public $tableOptions = ['class' => 'table table-hover'];
 
    // override styling of your data columns
    public $dataColumnClass = 'common\widgets\DoDataColumn';
 
    // override styling of your pager
    public $pager = [
        'options' => ['class' => 'pagination pagination-sm no-margin pull-right'],
        'firstPageCssClass' => 'first',
        'lastPageCssClass' => 'last',
        'prevPageCssClass' => 'prev',
        'nextPageCssClass' => 'next',
        'activePageCssClass' => 'active',
        'disabledPageCssClass' => 'disabled'
    ];
 
    // override styling of your sorter
    public $sorter = ['options' => ['class' => 'sorter']];
 
    // override other styles through these options
    public $options = ['class' => 'grid-view'];
    public $headerRowOptions = [];
    public $footerRowOptions = [];
    public $rowOptions = [];
}